#include <Arduino.h>
#include "ODriveTeensyCAN.h"
#include "SBUS.h"
#include <SimpleTimer.h>
#include <sensor_msgs/Joy.h>
#include <ackermann_msgs/AckermannDriveStamped.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <Adafruit_MCP4725.h>
#include <AutoPID.h>
#include <ros.h>
#include <Wire.h>
#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>
#include <dynamic_reconfigure/Config.h>
#include "FreqMeasure.h"


#define RPM_TIMEOUT 1000 // timeout on RPM to settle to zero 
#define throttle A22
#define RTD_led A18
#define ODRIVE_status_led A21
#define deadman_switch_led A19
#define button 26
// the switch pin for the enable of the reset button that runs the arm odrive full function
#define reset_toggle 12
#define enc_ticks_per_rotation 1024
#define CONTROL_LED 24
#define grayhill_A 14
#define grayhill_B 15

#define encoder_pin 2103

bool DEBUG = false;

// the max throttle allowed (max is 4096 which outputs to 3.3v on the DAC and 5v on the output of the op-amp)
float maxthrottle = 4095;
// the deadband on the controller (if the stick hasnt moved more than this, nothing will be output)
float controller_deadband = 0.01;

// +--------------------------------+
// |         State Machine          |
// |                                |
// |0 +--> Emergency/Startup        |
// |                                |
// |1 +--> Initialization           |
// |                                |
// |3 +--> Control (Ready to Drive) |
// +--------------------------------+
//Setting up PID


#define TIME_STEP 100

double OUTPUT_MIN = 0.0;
double OUTPUT_MAX = 10000.0;

double KP = 10.0;
double KI = 2.0;
double KD = 3.2;
volatile double current_rpm, set_rpm, throttle_out;
AutoPID speedPID(&current_rpm, &set_rpm, &throttle_out, OUTPUT_MIN, OUTPUT_MAX, KP, KI, KD);

float prevmsgx = -1.0;
float prevmsgy = -1.0;
float prevmsgz = -1.0;

int count;
int sum;
int control_state_ = 0;

//Grayhill Encoder
Encoder grayhill(grayhill_A, grayhill_B);
unsigned long previous_rpm_time = 0;
long prev_encoder_ticks_ = 0;
std_msgs::Float32 rpm;
std_msgs::Float32 thr_out;
std_msgs::Float32 current_steering;
geometry_msgs::Vector3 pid_coeffcients;

// Throttle MCP4725 DAC

Adafruit_MCP4725 digital_throttle;


// a SBUS object, which is on hardware serial port 2
SBUS x8r(Serial2);
// relay_in is the enable relay controlled by the teensy that turns on the key switch and powers on the emergency brake
int relay_in = 23; // (pin number)
// the max brake torque in newton-meters that gets applied at the shaft of the motor for the brake
float max_brake = 0.7;
// the max steering rotations (in rotations). this is absolute so actual range is -13 to 13 rotations in either direction
float max_steering = 13;

//ODRIVE STUFF
//on the odrive I am using node ids 3 and 5 (this is for the CAN connection to the odrive)
int axis_steering = 3;
int axis_braking = 5;

// controller values
int deadman;
float controller_steering;
float controller_throttle;
int arm_switch;
int mux_switch;


void target_set(const ackermann_msgs::AckermannDriveStamped &msg)
{
  if (mux_switch == 0)
  {
    digitalWrite(CONTROL_LED, LOW);
    //controller_throttle = msg.drive.speed;
    //set_rpm = msg.drive.speed*60/(.0254*11*PI);
    set_rpm = msg.drive.speed;
    controller_steering = max_steering * msg.drive.steering_angle * (2 / 3.14);

  }
}

void setPIDCoeffcients(geometry_msgs::Vector3 msg)
{
  if( (prevmsgx !=msg.x) ||(prevmsgy !=msg.y) || (prevmsgz !=msg.z)){
    speedPID.setGains((double)msg.x, (double)msg.y, (double)msg.z);
    prevmsgx = msg.x;
    prevmsgy = msg.y;
    prevmsgz = msg.z;
  }
  

}

ros::NodeHandle nh;
ros::Subscriber<geometry_msgs::Vector3> pid_sub("/PID_tune", &setPIDCoeffcients);
ros::Subscriber<ackermann_msgs::AckermannDriveStamped> sub("/control_output", &target_set);
ros::Publisher pub("/rpm", &rpm);
ros::Publisher steer_pub("/angle", &current_steering);
ros::Publisher thrpub("/throttle_out", &thr_out);


enum odrive_satus
{
  no_error = 0,
  startup = 1,
  error = 2
};

int odrive_st = startup;
ODriveTeensyCAN odrive;

SimpleTimer check_odrive;
SimpleTimer log_data;

bool armed = false;
bool deadman_switched = false;
bool emergency = false;

int current_throttle = 0;



void checkOdrive()
{
  if (odrive.GetAxisError(axis_braking) != 0 || odrive.GetAxisError(axis_steering) != 0)
  {
    odrive_st = error;
  }
}



unsigned long current_rpm_change_time;

void publishData()
{
  thr_out.data = throttle_out/OUTPUT_MAX;
  pub.publish(&rpm);
  thrpub.publish(&thr_out);
  steer_pub.publish(&current_steering);
}


void setup()
{
  current_rpm_change_time = 0;
  control_state_ = 1;
  
  // this means that the kart is in initialization state
  Wire.setSCL(33);
  Wire.setSDA(34);
  bool test2 = digital_throttle.begin(MCP4725_I2CADDR_DEFAULT, &Wire);
  //bool test = digital_throttle.setVoltage(4065, false, 100000UL);

  x8r.begin();
  analogWriteResolution(12);
  Serial.begin(9600);
  Serial.begin(57600);
  Serial4.begin(57600);
  pinMode(relay_in, OUTPUT);
  pinMode(button, INPUT);
  pinMode(reset_toggle, INPUT);
  pinMode(RTD_led, OUTPUT);
  pinMode(CONTROL_LED, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  //pinMode(ODRIVE_status_led,OUTPUT);
  pinMode(deadman_switch_led, OUTPUT);
  speedPID.setTimeStep(TIME_STEP);
  // speedPID.setGains(1, 1, 1);
  digitalWrite(relay_in, HIGH);
  FreqMeasure.begin();
  //check the odrive for axis errors every 250ms
  check_odrive.setInterval(250, checkOdrive);
  log_data.setInterval(100, publishData);
  rpm.data = 0;
  current_steering.data = 0;
  nh.getHardware()->setBaud(57600);
  nh.initNode();
  nh.subscribe(sub);
  nh.advertise(pub);
  nh.advertise(steer_pub);
  nh.advertise(thrpub);
}

void brake(float b_val)
{
  float val = max_brake * b_val;
  odrive.SetTorque(axis_braking, -val);
}

void throttle_control(float t_val)
{
  int controller_throttle = maxthrottle * t_val;
  if(abs(controller_throttle - current_throttle) > 2 && controller_throttle >= 0){
    digital_throttle.setVoltage(controller_throttle, false, 400000UL);
    current_throttle = controller_throttle;
    //Serial.println(current_throttle);
    //analogWrite(throttle, (int)controller_throttle);
  }
}
// request and verifies the status of the odrive in startup (setting the odrive to allow for control)
void arm_odrive()
{
  int requested_state;
  requested_state = ODriveTeensyCAN::AXIS_STATE_CLOSED_LOOP_CONTROL;
  odrive.RunState(axis_braking, requested_state);
  odrive.RunState(axis_steering, requested_state);
  while ((odrive.GetCurrentState(axis_braking) != ODriveTeensyCAN::AXIS_STATE_CLOSED_LOOP_CONTROL) && (odrive.GetCurrentState(axis_steering) != ODriveTeensyCAN::AXIS_STATE_CLOSED_LOOP_CONTROL))
  {
    delay(250);
  }
  odrive_st = no_error;
  analogWrite(ODRIVE_status_led, 4096);
}

// run the full calibration sequence of the odrive
void calibrate_odrive()
{
  int requested_state;

  requested_state = ODriveTeensyCAN::AXIS_STATE_FULL_CALIBRATION_SEQUENCE;
  odrive.RunState(axis_steering, requested_state);
  // odrive.RunState(axis_braking, requested_state);
  delay(200);

  int timeout_ms = 0;
  // while ((odrive.GetCurrentState(axis_braking) != ODriveTeensyCAN::AXIS_STATE_IDLE) && (odrive.GetCurrentState(axis_steering) != ODriveTeensyCAN::AXIS_STATE_IDLE))
  // {
  //   delay(500);
  //   timeout_ms += 500;
  // }
  while (odrive.GetCurrentState(axis_steering) != ODriveTeensyCAN::AXIS_STATE_IDLE)
  {
    delay(500);
    timeout_ms += 500;
  }
}

void odrive_reset()
{
  odrive.ClearErrors(axis_steering);
  delay(100);
  odrive.ClearErrors(axis_braking);
  delay(100);
  arm_odrive();
}

// run the calibration and arming sequence for the odrive
void armOdrivefull()
{
  odrive_reset();
  delay(200);

  // Calibrating steering and braking axises.
  calibrate_odrive();

  // Arming Odrive
  arm_odrive();
}

// the emergency state that gets switched to when the deadman gets released
// (and when it is working, the odrive status). once switched to, this emegency state cannot be escaped (requires a restart)
void emergency_state()
{
  control_state_ = 0;
  digitalWrite(relay_in, HIGH);
  digitalWrite(RTD_led, LOW);

  emergency = true;
}

// the state that only allows for steering, no throttle or brake
void idle_state(float controller_steering)
{
  digitalWrite(relay_in, LOW);
  throttle_control(0.0003);

  odrive.SetPosition(axis_steering, controller_steering);
  current_steering.data = (3.14 / 2) * (controller_steering / max_steering);
}

// the full control state that allows for steering and throttle and brake
void control_state(float controller_steering, float controller_throttle)
{
  control_state_ = 3;
  // turn on the key switch and power on the emag brake to hold it back
  digitalWrite(relay_in, LOW);
  current_steering.data = (3.14 / 2) * (controller_steering / max_steering);
  odrive.SetPosition(axis_steering, -controller_steering);

  if ((controller_throttle - controller_deadband) > 0)
  {
    brake(0);
    throttle_control(controller_throttle);
  }
  else if (controller_throttle < 0)
  {
    throttle_control(0.0003);
    brake(controller_throttle);
  }
  else
  {
    brake(0);
    throttle_control(0.0003);
  }
}

int getRPM()
{
  long encoder_ticks = grayhill.read();

  //this function calculates the motor's RPM based on encoder ticks and delta time
  unsigned long current_time = millis();
  unsigned long time_diff = current_time - previous_rpm_time;

  //convert the time from milliseconds to seconds
  double delta_minutes = (double)time_diff / 60000;
  unsigned long delta_ticks = encoder_ticks - prev_encoder_ticks_;

  //calculate wheel's speed (RPM)
  previous_rpm_time = current_time;
  prev_encoder_ticks_ = encoder_ticks;

  return (abs(delta_ticks / enc_ticks_per_rotation) / delta_minutes);
}

void loop()
{
  digitalWrite(relay_in, LOW);
 // check_odrive.run();
 log_data.run();
 

  // if (digitalRead(button) && digitalRead(reset_toggle) && control_state_ != 3)
  // {
  //   armOdrivefull();
  // }

  float channels[16];
  bool failSafe;
  bool lostFrame;

  // read the channel values of the RC receiver (16 channels in total)

  // channel 1 = throttle
  // channel 2 = steering
  // channel 5 = deadman switch
  // channel 6 = arm switch
  // channel 7 = mux switch
  if (x8r.readCal(&channels[0], &failSafe, &lostFrame))
  {
    for (int i = 0; i < 16; i++)
    {
      x8r.readCal(&channels[i], &failSafe, &lostFrame);
    }

    deadman = channels[4];
    arm_switch = channels[5];
    mux_switch = channels[6];
    if (mux_switch == -1)
    {
      digitalWrite(CONTROL_LED, HIGH);
      digitalWrite(LED_BUILTIN, HIGH);
      controller_steering = max_steering * channels[1];
      controller_throttle = channels[0];
      //Serial.println(controller_throttle);
      throttle_control(controller_throttle);
    } else {
      digitalWrite(CONTROL_LED, LOW);
    }
  }
  //   if (deadman == 0)
  //   {
  //     digitalWrite(deadman_switch_led, HIGH);
  //   }
  //   else
  //   {
  //     digitalWrite(deadman_switch_led, LOW);
  //   }

  //   if (odrive_st == no_error)
  //   {
  //     analogWrite(ODRIVE_status_led, 4096);
  //   }
  //   else
  //   {
  //     analogWrite(ODRIVE_status_led, 0);
  //   }

  //   if ((arm_switch == 0) && (odrive_st == startup || emergency) && mux_switch == -1)
  //   {
  //   //  arm_odrive();
  //   }

  //   //state machine
  //   if ((odrive_st == error || emergency) || (deadman_switched && !(deadman == 0)))
  //   {
  //     emergency_state();
  //   }
  //   else if (odrive_st == startup)
  //   {
  //     // Do nothing until startup finishes
  //     Serial4.println("in startup");
  //   }
  //   else if (odrive_st == no_error && !(deadman == 0) && !deadman_switched)
  //   {
  //     //Serial.println("in idle state");
  //     Serial4.println("steering");
  //     Serial4.println(controller_steering);
  //     Serial4.println("throttle");
  //     Serial4.println(controller_throttle);
  //     idle_state(controller_steering);
  //   }
  //   else if (odrive_st == no_error && deadman == 0)
  //   {
  //     //control_state_=3;
  //     // this means that the kart is in control
  //     control_state(controller_steering, controller_throttle);
  //     deadman_switched = true;
  //     digitalWrite(RTD_led, HIGH);
  //   }
  //   else
  //   {
  //     emergency_state();
  //   }
  // }


  // timeout on RPM to settle to zero 
  unsigned long test = millis();
  if((test - current_rpm_change_time) > RPM_TIMEOUT) {
    rpm.data = 0.0;
    
  }
  
  if (FreqMeasure.available()) {
    // average several reading together
    sum = sum + FreqMeasure.read();
    count = count + 1;
    if (count > 1) {
      float testRpm = FreqMeasure.countToFrequency(sum / count)*12;
      if(testRpm < 1500.0){
        rpm.data = testRpm;
        
        current_rpm_change_time = millis();
      }
      
      sum = 0;
      count = 0;

    }
  }
  if(mux_switch ==0) {
    throttle_control(((float)throttle_out/(float)OUTPUT_MAX));  
    speedPID.run();
  }
  
  //Serial.println(speedPID.getIntegral());
  current_rpm = (double)rpm.data;
  
  
  nh.spinOnce();
}
