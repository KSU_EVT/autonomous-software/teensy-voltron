#include <Arduino.h>
#include "ODriveTeensyCAN.h"
#include "SBUS.h"
#include <SimpleTimer.h>
#include <Adafruit_MCP4725.h>
#include <ros.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/Vector3.h>
#include <ackermann_msgs/AckermannDriveStamped.h>
#include <Wire.h>
#include "FreqMeasure.h"

#include <AutoPID.h>


#define RPM_TIMEOUT 500 // timeout on RPM to settle to zero 

#ifdef ARDUINO_TEENSY35
  #define throttle A22
  #define RTD_led A18
  #define ODRIVE_status_led A21
  #define deadman_switch_led A19
#elif ARDUINO_TEENSY40 //our Teensy 4.0 is just a test bench
  #define throttle A0
  #define RTD_led A1
  #define ODRIVE_status_led A2
  #define deadman_switch_led A3
#endif

#define button 26
// the switch pin for the enable of the reset button that runs the arm odrive full function
#define reset_toggle 12
#define CONTROL_LED 24
#define TIME_STEP 1

bool DEBUG = false;

// the max throttle allowed (max is 4096 which outputs to 3.3v on the DAC and 5v on the output of the op-amp)
float maxthrottle = 4095;
// the deadband on the controller (if the stick hasnt moved more than this, nothing will be output)
float controller_deadband = 0.01;

// +--------------------------------+
// |         State Machine          |
// |                                |
// |0 +--> Emergency/Startup        |
// |                                |
// |1 +--> Initialization           |
// |                                |
// |3 +--> Control (Ready to Drive) |
// +--------------------------------+
int control_state_ = 0;
int current_throttle = 0;
float controller_set_rpm_max = 500.0;
Adafruit_MCP4725 digital_throttle;

unsigned long previous_rpm_time = 0;
long prev_encoder_ticks_ = 0;

unsigned long current_rpm_change_time;
unsigned long lp_rpm_time;
float prev_rpm = 0;

double OUTPUT_MIN = -1.0;
double OUTPUT_MAX = 1.0;

double KP = 0.004;
double KI = 0.0001;
double KD = 1.0;
volatile double current_rpm, set_rpm, throttle_out;
AutoPID speedPID(&current_rpm, &set_rpm, &throttle_out, OUTPUT_MIN, OUTPUT_MAX, KP, KI, KD);

float prevmsgx = -1.0;
float prevmsgy = -1.0;
float prevmsgz = -1.0;




// a SBUS object, which is on hardware serial port 2
SBUS x8r(Serial2);
// relay_in is the enable relay controlled by the teensy that turns on the key switch and powers on the emergency brake
int relay_in = 28; // (pin number)
// the max brake torque in newton-meters that gets applied at the shaft of the motor for the brake
float max_brake = 0.35;
// the max steering rotations (in rotations). this is absolute so actual range is -13 to 13 rotations in either direction
float max_steering = 13;

//ODRIVE STUFF
//on the odrive I am using node ids 3 and 5 (this is for the CAN connection to the odrive)
int axis_steering = 3;
int axis_braking = 5;

// controller values
int deadman;
float controller_steering;
float controller_throttle;
int arm_switch;
int mux_switch;

enum odrive_satus
{
  no_error = 0,
  startup = 1,
  error = 2
};



int sum = 0;
int count = 0;


int odrive_st = startup;
ODriveTeensyCAN odrive;

SimpleTimer check_odrive;
SimpleTimer log_data;

bool armed = false;
bool deadman_switched = false;
bool emergency = false;

void target_set(const ackermann_msgs::AckermannDriveStamped &msg)
{
  if (mux_switch == 0)
  {
    digitalWrite(CONTROL_LED, LOW);
    set_rpm = msg.drive.speed;
    // set_rpm = (msg.drive.speed * 60) / (.0254 * 11); 
    if(set_rpm == 0)
    {
      speedPID.setIntegral(0);
    }
    controller_steering = max_steering * msg.drive.steering_angle * (2 / 3.14);
  }
}

void setPIDCoeffcients(geometry_msgs::Vector3 msg)
{
  if( (prevmsgx !=msg.x) ||(prevmsgy !=msg.y) || (prevmsgz !=msg.z)){
    speedPID.setGains((double)msg.x, (double)msg.y, (double)msg.z);
    prevmsgx = msg.x;
    prevmsgy = msg.y;
    prevmsgz = msg.z;
  }
  

}


void checkOdrive()
{
  if (odrive.GetAxisError(axis_braking) != 0 || odrive.GetAxisError(axis_steering) != 0)
  {
    odrive_st = error;
  }
}



std_msgs::Float32 thr_out;
std_msgs::Float32 current_steering;
geometry_msgs::Vector3 pid_coeffcients;
std_msgs::Float32 rpm;

ros::NodeHandle nh;
ros::Subscriber<geometry_msgs::Vector3> pid_sub("/PID_tune", &setPIDCoeffcients);
ros::Subscriber<ackermann_msgs::AckermannDriveStamped> sub("/control_output", &target_set);
ros::Publisher pub("/rpm", &rpm);
ros::Publisher steer_pub("/angle", &current_steering);
ros::Publisher thrpub("/throttle_out", &thr_out);


void publishData()
{

  thr_out.data = throttle_out;
  pub.publish(&rpm);
  thrpub.publish(&thr_out);
  steer_pub.publish(&current_steering);

};

void setup()
{
  control_state_ = 1;
  // this means that the kart is in initialization state

  x8r.begin();
  analogWriteResolution(12);
  Serial.begin(57600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(relay_in, OUTPUT);
  pinMode(button, INPUT);
  pinMode(reset_toggle, INPUT);
  pinMode(RTD_led, OUTPUT);
  pinMode(CONTROL_LED, OUTPUT);
  pinMode(ODRIVE_status_led,OUTPUT);
  pinMode(deadman_switch_led, OUTPUT);
  
  digitalWrite(relay_in, HIGH);

  digital_throttle.begin(MCP4725_I2CADDR_DEFAULT, &Wire);
  digital_throttle.setVoltage(0, false, 100000UL);
  //check the odrive for axis errors every 250ms
  check_odrive.setInterval(250, checkOdrive);
  log_data.setInterval(50, publishData);

  Wire.setSCL(19);
  Wire.setSDA(18);

  nh.getHardware()->setBaud(57600);
  nh.initNode();
  nh.subscribe(sub);
  nh.subscribe(pid_sub);
  nh.advertise(pub);
  nh.advertise(steer_pub);
  nh.advertise(thrpub);
  FreqMeasure.begin();

}

void brake(float b_val)
{
  float val = max_brake * b_val;
  odrive.SetTorque(axis_braking, -val);
}

void throttle_control(float t_val)
{
  int controller_throttle = maxthrottle * t_val;
  if(controller_throttle >= 0){
    bool test = digital_throttle.setVoltage(controller_throttle, false);
    current_throttle = controller_throttle;
    //Serial.println(current_throttle);
    //analogWrite(throttle, (int)controller_throttle);
  } else {
    bool test = digital_throttle.setVoltage(0, false);
  }

}

// request and verifies the status of the odrive in startup (setting the odrive to allow for control)
void arm_odrive()
{
  digitalWrite(relay_in, HIGH);
  int requested_state;

  digitalWrite(LED_BUILTIN, LOW);

  delay(100);
  delay(100);
  delay(100);
  digitalWrite(LED_BUILTIN, HIGH);
  while ((odrive.GetCurrentState(axis_braking) != ODriveTeensyCAN::AXIS_STATE_CLOSED_LOOP_CONTROL) && (odrive.GetCurrentState(axis_steering) != ODriveTeensyCAN::AXIS_STATE_CLOSED_LOOP_CONTROL))
  {
    digitalWrite(LED_BUILTIN, LOW);
    requested_state = ODriveTeensyCAN::AXIS_STATE_CLOSED_LOOP_CONTROL;
    odrive.RunState(axis_braking, requested_state);
    delay(100);
    odrive.RunState(axis_steering, requested_state);
    delay(100);
  }
  digitalWrite(LED_BUILTIN, HIGH);
  
  odrive_st = no_error;
  // analogWrite(ODRIVE_status_led, 4096);
}

// run the full calibration sequence of the odrive
void calibrate_odrive()
{
  int requested_state;
  digitalWrite(LED_BUILTIN, LOW);

  delay(500);
  digitalWrite(LED_BUILTIN, HIGH);

  requested_state = ODriveTeensyCAN::AXIS_STATE_FULL_CALIBRATION_SEQUENCE;
  odrive.RunState(axis_steering, requested_state);
  delay(100);
  odrive.RunState(axis_braking, requested_state);
  delay(100);

  while ((odrive.GetCurrentState(axis_steering) != ODriveTeensyCAN::AXIS_STATE_IDLE) && (odrive.GetCurrentState(axis_steering) != ODriveTeensyCAN::AXIS_STATE_IDLE))
  {
    delay(100);
    
  }
  delay(100);
  
  requested_state = ODriveTeensyCAN::AXIS_STATE_HOMING;
  odrive.RunState(axis_steering, requested_state);
  delay(200);
  
  while (odrive.GetCurrentState(axis_steering) != ODriveTeensyCAN::AXIS_STATE_IDLE)
  {
    delay(100);
    
  }

  digitalWrite(LED_BUILTIN, LOW);
}

void odrive_reset()
{

  odrive.ClearErrors(axis_steering);
  delay(100);
  odrive.ClearErrors(axis_braking);
  delay(100);
}

// run the calibration and arming sequence for the odrive
void armOdrivefull()
{
  odrive_reset();
  digitalWrite(LED_BUILTIN, HIGH);
  delay(200);

  // Calibrating steering and braking axises.
  calibrate_odrive();

  // Arming Odrive
  arm_odrive();
}

// the emergency state that gets switched to when the deadman gets released
// (and when it is working, the odrive status). once switched to, this emegency state cannot be escaped (requires a restart)
void emergency_state()
{
  control_state_ = 0;
  digitalWrite(relay_in, HIGH);
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(RTD_led, LOW);

  emergency = true;
}

// the state that only allows for steering, no throttle or brake
void idle_state(float controller_steering)
{
  // digitalWrite(relay_in, LOW);
  digitalWrite(LED_BUILTIN, LOW);
  throttle_control(0.0003);
  odrive.SetPosition(axis_steering, controller_steering);
  current_steering.data = (3.14 / 2) * (controller_steering / max_steering);
}

// the full control state that allows for steering and throttle and brake
void control_state(float controller_steering, float controller_throttle)
{
  control_state_ = 3;
  // turn on the key switch and power on the emag brake to hold it back
  digitalWrite(relay_in, LOW);
  digitalWrite(LED_BUILTIN, HIGH);
  current_steering.data = (3.14 / 2) * (controller_steering / max_steering);
  odrive.SetPosition(axis_steering, controller_steering);

  if ((controller_throttle - controller_deadband) > 0)
  {
    brake(0);
    throttle_control(controller_throttle);
  }
  else if (controller_throttle < 0)
  {
    throttle_control(0.0003);
    brake(controller_throttle);
  }
  else
  {
    brake(0);
    throttle_control(0.0003);
  }
}

void loop()
{
 
  
  log_data.run();

  
  
  if (digitalRead(button) && digitalRead(reset_toggle))
  {
    digitalWrite(LED_BUILTIN, HIGH);
    armOdrivefull();
    digitalWrite(LED_BUILTIN, LOW);
  }

  float channels[16];
  bool failSafe;
  bool lostFrame;

  // read the channel values of the RC receiver (16 channels in total)

  // channel 1 = throttle
  // channel 2 = steering
  // channel 5 = deadman switch
  // channel 6 = arm switch
  // channel 7 = mux switch

  if (x8r.readCal(&channels[0], &failSafe, &lostFrame))
  {
    
    for (int i = 0; i < 16; i++)
    {
      x8r.readCal(&channels[i], &failSafe, &lostFrame);
    }

    deadman = channels[4];
    arm_switch = channels[5];
    mux_switch = channels[6];
    if (mux_switch == -1)
    {
      digitalWrite(CONTROL_LED, HIGH);
      // speedPID.run();
      speedPID.reset();
    controller_steering = max_steering * channels[1];
      controller_throttle = channels[0];
    } 
    else if(mux_switch == 0) {
      //throttle_control((float)throttle_out);
      speedPID.run();
      controller_throttle = throttle_out;
      // digitalWrite(relay_in, LOW);  
      // control_state((float)controller_steering, (float)(throttle_out));
      // if (!speedPID.atSetPoint(5)){
      
      // }
      // else{
      //   speedPID.run();
      //   // throttle_out = 0;
      //   speedPID.setIntegral(0);
      // }
    }
    else {
      // TODO: remove this later on, this
      // set_rpm =  controller_set_rpm_max * channels[0];
      controller_steering = max_steering * channels[1];
      digitalWrite(CONTROL_LED, LOW);
    }

    if (deadman == 0)
    {
      
      digitalWrite(deadman_switch_led, HIGH);
    }
    else
    {
      digitalWrite(LED_BUILTIN, LOW);
      digitalWrite(deadman_switch_led, LOW);
    }

    // if (odrive_st == no_error)
    // {
    //   analogWrite(ODRIVE_status_led, 4096);
    // }
    // else
    // {
    //   analogWrite(ODRIVE_status_led, 0);
    // }

    if ((arm_switch == 0) && mux_switch == -1)
    {
      arm_odrive();
    }

    //state machine
    if ((emergency) || (deadman_switched && !(deadman == 0)))
    {
      emergency_state();
    }
    else if (!(deadman == 0) && !deadman_switched)
    {

      idle_state(controller_steering);
    }
    else if ( (deadman == 0))
    {
      control_state_=3;
      // this means that the kart is in control
      digitalWrite(LED_BUILTIN, HIGH);
      control_state(controller_steering, controller_throttle);
      deadman_switched = true;
      digitalWrite(RTD_led, HIGH);
    }
    else
    {
      emergency_state();
    }



    unsigned long test = millis();
    if((test - current_rpm_change_time) > RPM_TIMEOUT) {
      rpm.data = 0;
    }
    
    if (FreqMeasure.available()) {
      // average several reading together
      sum = sum + FreqMeasure.read();
      count = count + 1;
      current_rpm_change_time = millis();
      if (count > 1) {
          float testRpm = FreqMeasure.countToFrequency(sum / count)*12;
          if ( testRpm - prev_rpm < 100)
          {
            rpm.data = testRpm;
            current_rpm = (double)testRpm;
            prev_rpm = testRpm;
          }
          // Serial.printf("Current RPM: %f \n", testRpm);
        sum = 0;
        count = 0;
        prev_rpm = testRpm;
      }
    }
    

    

  }
  
  nh.spinOnce();
}